﻿double operand1 = ReadDouble("Enter operand");
string op = ReadOperator("Enter operator");

if (IsBinaryOperator(op))
{
    double operand2 = ReadDouble("Enter operand");

    Console.WriteLine(Calculate(operand1, op, operand2));
}
else
{
    Console.WriteLine(Calculate(operand1, op, null));
}

bool IsOperatorSupported(string op)
{
    return IsBinaryOperator(op) || IsUnaryOperator(op);
}

bool IsBinaryOperator(string op)
{
    return op == "+" || op == "-" || op == "*" || op == "/";
}

bool IsUnaryOperator(string op)
{
    return op == "sqrt";
}

double Calculate(double operand1, string op, double? operand2)
{
    double result;

    if (operand2 == null)
    {
        result = CalculateUnary(operand1, op);
    }
    else
    {
        result = CalculateBinary(operand1, op, operand2.Value);
    }

    return result;
}

double CalculateUnary(double a, string op)
{
    switch (op)
    {
        case "sqrt":
            return Math.Sqrt(a);
        default:
            throw new NotSupportedException("Operator not supported: " + op);
    }
}

double CalculateBinary(double a, string op, double b)
{
    switch (op)
    {
        case "+":
            return a + b;
        case "-":
            return a - b;
        case "*":
            return a * b;
        case "/":
            return a / b;
        default:
            throw new NotSupportedException("Operator not supported: " + op);
    }
}

string ReadOperator(string message)
{
    Console.WriteLine(message);

    bool has_succeeded;
    string op;

    do
    {
        op = Console.ReadLine()!;

        has_succeeded = IsOperatorSupported(op);

        if (!has_succeeded)
        {
            Console.WriteLine("Operator not supported. Try again please.");
        }
    } while (!has_succeeded);

    return op;
}

double ReadDouble(string message)
{
    Console.WriteLine(message);

    string? str;
    bool has_succeeded;
    double result;

    do 
    {
        str = Console.ReadLine();

        has_succeeded = double.TryParse(str, out result);

        if (!has_succeeded)
        {
            Console.WriteLine("Double expected. Try again please.");
        }
    } while (!has_succeeded);
        
    return result;
}