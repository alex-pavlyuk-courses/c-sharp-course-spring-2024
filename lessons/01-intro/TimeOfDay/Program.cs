﻿using System;

DateTime now = DateTime.Now;

string timeOfDay;

if (now.Hour < 12)
{
    timeOfDay = "morning";
}
else if (now.Hour < 17)
{
    timeOfDay = "afternoon";
}
else if (now.Hour < 22)
{
    timeOfDay = "evening";
}
else
{
    timeOfDay = "night";
}

Console.WriteLine("Good " + timeOfDay + ", World!");


