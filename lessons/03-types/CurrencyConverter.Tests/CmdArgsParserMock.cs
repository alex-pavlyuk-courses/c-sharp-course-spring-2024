namespace CurrencyConverter.Tests;

internal class CmdArgsParserMock : ICmdArgsParser
{
    public List<string[]> InvocationsArgs { get; } = new List<string[]>();

    public ConversionRequest? NextParseResult { get; set; } = null;

    public int PrintUsageCount { get; set; } = 0;

    public ConversionRequest? TryParseCmdArgs(string[] args)
    {
        InvocationsArgs.Add(args);

        return NextParseResult;
    }

    public void PrintUsage()
    {
        PrintUsageCount++;
    }
}