
namespace CurrencyConverter.Tests;

public class CmdArgsParserTests
{
    [Fact]
    public void HappyPath()
    {
        //  arrange
        var console = new ConsoleMock();
        var cmdArgsParser = new CmdArgsParser(console);

        //  act
        var actual = cmdArgsParser.TryParseCmdArgs(["--from=EUR", "--to=USD", "--amount=100"])!;

        //  assert
        Assert.Equal("EUR", actual.FromCurrency);
        Assert.Equal("USD", actual.ToCurrency);
        Assert.Equal(100m, actual.FromAmount);
    }

    [Theory]
    [InlineData(["--to=USD", "--amount=100"])]
    [InlineData(["--from=EUR", "--amount=100"])]
    [InlineData(["--from=EUR", "--to=USD"])]
    [InlineData(["--form=EUR", "--to=USD", "--amount=100"])]
    [InlineData(["--from=EUR", "--ot=USD", "--sfw=100"])]
    [InlineData(["-from=EUR", "--to=USD", "--amount=100"])]
    [InlineData(["--from=EUR", "-to=USD", "--amount=100"])]
    [InlineData(["--from=EUR", "--to=USD", "-amount=100"])]
    [InlineData(["--from=EUR", "--to=USD", "--amount=10.sdf.0"])]
    public void ErrorCases(params string[] args)
    {
        //  arrange
        var console = new ConsoleMock();
        var cmdArgsParser = new CmdArgsParser(console);

        //  act
        var actual = cmdArgsParser.TryParseCmdArgs(args)!;

        //  assert
        Assert.Null(actual);
    }

    [Fact]
    public void TestPrintUsage()
    {
        //  arrange
        var console = new ConsoleMock();
        var cmdArgsParser = new CmdArgsParser(console);

        //  act
        cmdArgsParser.PrintUsage();

        //  assert
        Assert.Equal("Usage: dotnet CurrencyConverter.dll --from=EUR --to=USD --amount=100", console.InvocationArgs[0]);
    }
}