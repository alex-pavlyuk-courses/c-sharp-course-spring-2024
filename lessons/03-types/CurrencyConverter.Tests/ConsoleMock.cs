namespace CurrencyConverter.Tests;

public class ConsoleMock : IConsole
{
    public List<string> InvocationArgs { get; } = new List<string>();

    public void WriteLine(string str)
    {
        InvocationArgs.Add(str);
    }
}
