namespace CurrencyConverter.Tests;

public class ConversionRateProviderMock : IConversionRateProvider
{
    public List<string[]> InvocationsArgs { get; } = new List<string[]>();

    public decimal NextResult { get; set; }

    public decimal GetConversionRate(string fromCurrency, string toCurrency)
    {
        InvocationsArgs.Add([fromCurrency, toCurrency]);

        return NextResult;
    }
}
