namespace CurrencyConverter.Tests;

public class ConverterMock : IConverter
{
    public List<ConversionRequest> InvocationsArgs { get; } = new List<ConversionRequest>();

    public ConversionResult NextResult { get; set; }

    public ConversionResult ConvertCurrency(ConversionRequest conversionRequest)
    {
        InvocationsArgs.Add(conversionRequest);

        return NextResult;
    }
}
