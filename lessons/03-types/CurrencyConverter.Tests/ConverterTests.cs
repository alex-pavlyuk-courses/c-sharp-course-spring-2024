namespace CurrencyConverter.Tests;

public class ConverterTests
{
    [Fact]
    public void TestConvertCurrencyGetRate()
    {
        //  arrange
        var request = new ConversionRequest("RUB", "USD", 2331m);
        var conversionRateProvider = new ConversionRateProviderMock();
        var converter = new Converter(conversionRateProvider);

        conversionRateProvider.NextResult = 7371.137m;

        //  act
        var result = converter.ConvertCurrency(request);

        //  assert
        Assert.Single(conversionRateProvider.InvocationsArgs);
        Assert.Equal("RUB", conversionRateProvider.InvocationsArgs[0][0]);
        Assert.Equal("USD", conversionRateProvider.InvocationsArgs[0][1]);
    }

    [Fact]
    public void TestConvertCurrencyCalc()
    {
        //  arrange
        var request = new ConversionRequest("RUB", "USD", 2331m);
        var conversionRateProvider = new ConversionRateProviderMock();
        var converter = new Converter(conversionRateProvider);

        conversionRateProvider.NextResult = 7371.137m;

        //  act
        var result = converter.ConvertCurrency(request);

        //  assert
        Assert.Equal(7371.137m * 2331m, result.ToAmount);
        Assert.Equal(2331m, result.FromAmount);
        Assert.Equal("RUB", result.FromCurrency);
        Assert.Equal("USD", result.ToCurrency);
    }
}
