namespace CurrencyConverter.Tests;

public class CurrencyConverterAppTests
{
    [Fact]
    public void TestHappyPath()
    {
        //  arrange
        var cmdArgsParser = new CmdArgsParserMock();
        var converter = new ConverterMock();
        var console = new ConsoleMock();
        var app = new CurrencyConverterApp(cmdArgsParser, converter, console);

        cmdArgsParser.NextParseResult = new ConversionRequest("aaa", "bbb", 777m);
        converter.NextResult = new ConversionResult("aaa", "bbb", 777m, 12374m);

        //  act
        app.Run(["a", "b", "c"]);

        //  assert
        Assert.Single(console.InvocationArgs);
        Assert.Equal("777 aaa = 12374 bbb", console.InvocationArgs[0]);
    }

    [Fact]
    public void TestBadInput()
    {
        //  arrange
        var cmdArgsParser = new CmdArgsParserMock();
        var converter = new ConverterMock();
        var console = new ConsoleMock();
        var app = new CurrencyConverterApp(cmdArgsParser, converter, console);

        cmdArgsParser.NextParseResult = null;

        //  act
        app.Run(["a", "b", "c"]);

        //  assert
        Assert.Equal(1, cmdArgsParser.PrintUsageCount);
        Assert.Empty(converter.InvocationsArgs);
    }
}
