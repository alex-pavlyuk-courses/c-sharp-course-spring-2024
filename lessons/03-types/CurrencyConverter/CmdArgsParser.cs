namespace CurrencyConverter;

public class CmdArgsParser : ICmdArgsParser
{
    private readonly IConsole console;

    public CmdArgsParser(IConsole console)
    {
        this.console = console;
    }

    public ConversionRequest? TryParseCmdArgs(string[] args)
    {
        string? fromCurrency = TryParseCmdArg(args, "from");
        string? toCurrency = TryParseCmdArg(args, "to");
        string? fromAmountStr = TryParseCmdArg(args, "amount");
        ConversionRequest? result = fromCurrency != null && toCurrency != null && decimal.TryParse(fromAmountStr, out decimal fromAmount)
            ? new ConversionRequest(fromCurrency, toCurrency, fromAmount)
            : null;

        if (result == null)
        {
            PrintUsage();
        }

        return result;
    }

    string? TryParseCmdArg(string[] args, string argName)
    {
        argName = $"--{argName}=";
        string? result = null;

        foreach (var arg in args)
        {
            if (arg.StartsWith(argName))
            {
                result = arg.Substring(argName.Length);
                break;
            }
        }

        return result;
    }

    public void PrintUsage()
    {
        console.WriteLine("Usage: dotnet CurrencyConverter.dll --from=EUR --to=USD --amount=100");
    }
}
