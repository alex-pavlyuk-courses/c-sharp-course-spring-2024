
public class ConversionRequest
{
    public string FromCurrency { get; }

    public string ToCurrency { get; }
    
    public decimal FromAmount { get; }

    public ConversionRequest(string fromCurrency, string toCurrency, decimal fromAmount)
    {
        FromCurrency = fromCurrency;
        ToCurrency = toCurrency;
        FromAmount = fromAmount;
    }
}