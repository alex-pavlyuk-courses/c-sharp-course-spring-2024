



public class ConversionResult
{
    public string FromCurrency { get; }
    public string ToCurrency { get; }
    public decimal FromAmount { get; }
    public decimal ToAmount { get; }

    public ConversionResult(string fromCurrency, string toCurrency, decimal fromAmount, decimal toAmount)
    {
        FromCurrency = fromCurrency;
        ToCurrency = toCurrency;
        FromAmount = fromAmount;
        ToAmount = toAmount;
    }
}
