namespace CurrencyConverter;

public class Converter : IConverter
{
    private readonly IConversionRateProvider conversionRateProvider;

    public Converter(IConversionRateProvider conversionRateProvider)
    {
        this.conversionRateProvider = conversionRateProvider;
    }

    public ConversionResult ConvertCurrency(ConversionRequest conversionRequest)
    {
        decimal conversionRate = conversionRateProvider.GetConversionRate(conversionRequest.FromCurrency, conversionRequest.ToCurrency);

        decimal toAmount = conversionRequest.FromAmount * conversionRate;

        return new ConversionResult(conversionRequest.FromCurrency, conversionRequest.ToCurrency, conversionRequest.FromAmount, toAmount);
    }

}
