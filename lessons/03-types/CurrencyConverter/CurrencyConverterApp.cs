namespace CurrencyConverter;

public class CurrencyConverterApp
{
    private readonly ICmdArgsParser cmdArgsParser;
    private readonly IConverter converter;
    private readonly IConsole console;

    public CurrencyConverterApp(ICmdArgsParser cmdArgsParser, IConverter converter, IConsole console)
    {
        this.cmdArgsParser = cmdArgsParser;
        this.converter = converter;
        this.console = console;
    }

    public void Run(string[] args)
    {
        var request = cmdArgsParser.TryParseCmdArgs(args);

        if (request != null)
        {
            var conversionResult = converter.ConvertCurrency(request);

            PrintConversionResult(conversionResult);
        }
        else
        {
            cmdArgsParser.PrintUsage();
        }
    }

    void PrintConversionResult(ConversionResult conversionResult)
    {
        string output = string.Format(
            "{0} {1} = {2} {3}",
            conversionResult.FromAmount,
            conversionResult.FromCurrency,
            conversionResult.ToAmount,
            conversionResult.ToCurrency);

        console.WriteLine(output);
    }
}
