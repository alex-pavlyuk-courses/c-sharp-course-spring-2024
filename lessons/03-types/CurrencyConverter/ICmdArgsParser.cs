namespace CurrencyConverter;

public interface ICmdArgsParser
{
    ConversionRequest? TryParseCmdArgs(string[] args);

    void PrintUsage();
}