namespace CurrencyConverter;

public interface IConsole
{
    void WriteLine(string str);
}
