namespace CurrencyConverter;

public interface IConversionRateProvider
{
    decimal GetConversionRate(string fromCurrency, string toCurrency);
}
