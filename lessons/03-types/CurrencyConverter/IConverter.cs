namespace CurrencyConverter;

public interface IConverter
{
    ConversionResult ConvertCurrency(ConversionRequest conversionRequest);
}