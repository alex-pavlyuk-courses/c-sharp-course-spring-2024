﻿using CurrencyConverter;

public class Program
{
    public static void Main(string[] args)
    {
        var console = new ConsoleWrapper();
        var argsParser = new CmdArgsParser(console);
        var conversionRateProvider = new ConversionRateProvider();
        var converter = new Converter(conversionRateProvider);
        var app = new CurrencyConverterApp(argsParser, converter, console);

        app.Run(args);
    }
}