﻿using Npgsql;

string password = Environment.GetEnvironmentVariable("POSTGRES_CONNECTION_STRING_PASSWORD");

var connectionString = $"Host=127.0.0.1:5432;Username=postgres;Password={password};Database=my_db_for_test_prosto_sprosit";
var dataSource = NpgsqlDataSource.Create(connectionString);

// long id = InsertGood("Пирожок с мясом", 100, 76);
// Console.WriteLine("Id of inserted row: {0}", id);

var goods = SelectGoods();

foreach (var good in goods)
{
    Console.WriteLine(good);
}


long InsertGood(string title, int availableQuantity, decimal price)
{
    // Insert some data
    string sql = @"
        INSERT INTO public.goods(
            title, available_quantity, price)
            VALUES ($1, $2, $3)
            RETURNING id;";

    using (var cmd = dataSource.CreateCommand(sql))
    {
        cmd.Parameters.AddWithValue(title);
        cmd.Parameters.AddWithValue(availableQuantity);
        cmd.Parameters.AddWithValue(price);

        return (long)cmd.ExecuteScalar()!;

    }
}

List<Good> SelectGoods()
{
    var result = new List<Good>();
    string sql = @"
        SELECT 
            id, 
            title, 
            available_quantity, 
            price 
        FROM public.goods;";

    // Retrieve all rows
    using (var cmd = dataSource.CreateCommand(sql))
    using (var reader = cmd.ExecuteReader())
    {
        while (reader.Read())
        {
            var good = new Good
            {
                Id = reader.GetInt64(0),
                Title = reader.GetString(1),
                AvailableQuantity = reader.GetInt32(2),
                Price = reader.GetDecimal(3)
            };

            result.Add(good);
        }
    }

    return result;
}

class Good 
{
    public long Id { get; set; }

    public string Title { get; set; }

    public int AvailableQuantity { get; set; }

    public decimal Price { get; set; }

    override public string ToString()
    {
        return $"Id: {Id}, Title: {Title}, AvailableQuantity: {AvailableQuantity}, Price: {Price}";
    }
}