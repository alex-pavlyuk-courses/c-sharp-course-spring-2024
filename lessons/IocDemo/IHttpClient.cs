namespace IocDemo;

public interface IHttpClient
{
    HttpResponseMessage Get(string url);
}
