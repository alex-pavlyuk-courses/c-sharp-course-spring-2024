namespace IocDemo;

public interface ILogger
{
    public void Log(string message);
}
