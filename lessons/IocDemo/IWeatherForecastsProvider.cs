namespace IocDemo;

public interface IWeatherForecastsProvider
{
    WeatherForecast[] GetWeatherForecasts();
}
