﻿// See https://aka.ms/new-console-template for more information
using IocDemo;
using Microsoft.Extensions.DependencyInjection;




//  IoC containers - inversion of control containers
//  DI - Dependency Injection

IServiceCollection services = new ServiceCollection()
    .AddTransient<IWeatherForecastsProvider, GismeteoWeatherForecastsProvider>()
    .AddTransient<IHttpClient, IocDemo.HttpClient>()
    .AddSingleton<ILogger, FileLogger>();


var serviceProvider = services.BuildServiceProvider();

var weatherForecastsProvider = serviceProvider.GetService<IWeatherForecastsProvider>();

Console.WriteLine("Hello, World!");


void CreateByHand()
{
    ILogger logger = new FileLogger();
    IHttpClient httpClient = new IocDemo.HttpClient(logger);
    IWeatherForecastsProvider weatherForecastsProvider = new GismeteoWeatherForecastsProvider(httpClient, logger);
}

