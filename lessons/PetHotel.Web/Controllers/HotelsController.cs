
using Microsoft.AspNetCore.Mvc;
using PetHotel.Web.Models;
using PetHotel.Web.Models.DataAccess;

namespace PetHotel.Web.Controllers;

public class HotelsController : Controller
{
    private readonly IPetHotelDataAccess dataAccess;

    public HotelsController(IPetHotelDataAccess dataAccess)
    {
        this.dataAccess = dataAccess;
    }

    public IActionResult Index()
    {
        return View(dataAccess.GetHotels());
    }

    public IActionResult Details(long id)
    {
        Hotel? match = dataAccess.GetHotel(id);

        return match == null ? NotFound() : View(match);
    }

    public IActionResult Create()
    {
        return View();
    }

    [HttpPost]
    public IActionResult Create(Hotel hotel)
    {
        if (!ModelState.IsValid)
        {
            return View(hotel);
        }

        dataAccess.AddHotel(hotel);

        return RedirectToAction(nameof(Index));
    }

    public IActionResult Edit(long id)
    {
        Hotel? match = dataAccess.GetHotel(id);

        return match == null ? NotFound() : View(match);
    }

    [HttpPost]
    public IActionResult Edit(long id, Hotel hotel)
    {
        if (!ModelState.IsValid)
        {
            return View(hotel);
        }

        dataAccess.UpdateHotel(id, hotel);

        return RedirectToAction(nameof(Details), new { id });
    }

    public IActionResult Delete(long id)
    {
        var match = dataAccess.GetHotel(id);

        return match == null ? NotFound() : View(match);
    }

    [HttpPost]
    public IActionResult ConfirmDelete(long id)
    {
        dataAccess.DeleteHotel(id);

        return RedirectToAction(nameof(Index));
    }

}
