namespace PetHotel.Web.Models.DataAccess;

public interface IPetHotelDataAccess
{
    IEnumerable<Hotel> GetHotels();

    Hotel? GetHotel(long id);
    void AddHotel(Hotel hotel);
    void UpdateHotel(long id, Hotel hotel);
    void DeleteHotel(long id);
}