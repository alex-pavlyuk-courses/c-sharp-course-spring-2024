using System.Text.RegularExpressions;

namespace PetHotel.Web.Models.DataAccess;

public class PetHotelInMemoryDataAccess : IPetHotelDataAccess
{
    private readonly List<Hotel> hotels =
    [
        new Hotel { Id = 1, Name = "Hotel 1", Address = "Address 1"},
        new Hotel { Id = 2, Name = "Hotel 2", Address = "Address 2"},
        new Hotel { Id = 3, Name = "Hotel 3", Address = "Address 3"},
    ];

    public IEnumerable<Hotel> GetHotels()
    {
        return hotels;
    }

    public Hotel? GetHotel(long id)
    {
        Hotel? match = null;

        foreach (var hotel in hotels)
        {
            if (hotel.Id == id)
            {
                match = hotel;
                break;
            }
        }

        return match;
    }

    public void AddHotel(Hotel hotel)
    {
        hotels.Add(hotel);

        hotel.Id = hotels.Count;
    }

    public void UpdateHotel(long id, Hotel hotel)
    {
        var match = GetHotel(id);

        if (match != null)
        {
            match.Name = hotel.Name;
            match.Address = hotel.Address;
        }
    }

    public void DeleteHotel(long id)
    {
        bool removed = false;

        for (int i = 0; i < hotels.Count && !removed; i++)
        {
            if (hotels[i].Id == id)
            {
                hotels.RemoveAt(i);
                removed = true;
            }
        }

        if (!removed)
        {
            throw new Exception($"Hotel with id {id} not found");
        }
    }
}
