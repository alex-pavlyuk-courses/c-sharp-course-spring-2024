using System.ComponentModel.DataAnnotations;

namespace PetHotel.Web.Models;

public class Hotel
{
    public long Id { get; set; }

    [StringLength(maximumLength: 128, MinimumLength = 3)]
    public string? Name { get; set; }

    public string? Address { get; set; }
}
