namespace ReflectionDemo;

public class Employee
{
    [JsonProperty("fullname")]
    public string? Name { get; set; }

    public int Age { get; set; }

    public string? Position { get; set; }

    public Employee? Manager { get; set; }
}
