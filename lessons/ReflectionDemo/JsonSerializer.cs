
using System.Reflection;

namespace ReflectionDemo;

public class JsonSerializer
{
    public static string Serialize(object obj)
    {
        var props = obj.GetType().GetProperties();
        var fields = new string[props.Length];

        for (int i = 0; i < props.Length; i++)
        {
            string name = GetJsonPropertyName(props[i]);
            var value = props[i].GetValue(obj);
            string jsonValue = ConvertValueToJson(value);

            fields[i] = $"    \"{name}\": {jsonValue}";
        }

        return "{\n" + string.Join(",\n", fields) + "\n}";
    }

    private static string GetJsonPropertyName(PropertyInfo propertyInfo)
    {
        var attr = propertyInfo.GetCustomAttribute<JsonPropertyAttribute>();
        
        return attr == null 
            ? propertyInfo.Name
            : attr.Name;
    }

    private static string ConvertValueToJson(object? value)
    {
        string result;

        if (value == null)
        {
            result = "null";
        }
        else if (value is string)
        {
            result = $"\"{value}\"";
        }
        else if (value.GetType().IsClass)
        {
            result = Serialize(value);
        }
        else
        {
            result = value.ToString()!;
        }

        return result;
    }
}