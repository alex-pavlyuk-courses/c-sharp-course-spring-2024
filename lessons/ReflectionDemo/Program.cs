﻿// See https://aka.ms/new-console-template for more information
using System.Reflection;
using ReflectionDemo;

Console.WriteLine("===============");
Console.WriteLine("Reflection Demo");
Console.WriteLine("===============");

ShowBasics();

ShowJsonSerializer();

Assembly asm = Assembly.LoadFile("some asm.dll");



void ShowBasics()
{
    Type employeeType = typeof(Employee);

    ConstructorInfo? ctor = employeeType.GetConstructor([]);

    object? employee = ctor?.Invoke([]);

    PropertyInfo[] props = employeeType.GetProperties();

    foreach (var prop in props)
    {
        if (prop.Name == nameof(Employee.Name))
        {
            prop.SetValue(employee, "Ivan");
        }
    }
}

void ShowJsonSerializer()
{
    var manager = new Employee { Name = "Pesho", Age = 35, Position = "Manager" };
    var employee = new Employee { Name = "Ivan", Age = 25, Position = "Developer", Manager = manager };
    string json = JsonSerializer.Serialize(employee);

    Console.WriteLine(json);
}